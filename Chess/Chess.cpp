#include <iostream>
#include <conio.h> //consola knihovna
#include <Windows.h>
using namespace std;
bool gameover;
const int width = 20;
const int height = 20;
int x, y, fruitX, fruitY, score;
enum edirection { STOP = 0, LEFT, RIGHT, UP, DOWN};
edirection dir;

void setup()
{
	gameover = false;
	dir = STOP; // hra nebezi
	x = width / 2; // zacinam v strede 
	y = height / 2;
	fruitX = rand() % width; // random na mape ovocie
	fruitY = rand() % height;
	score = 0; // zacinam s 0 skore
}
void Draw()
{
	system("cls"); // vycistene
	for (int i = 0; i < width + 2; i++)
		cout << "#";
	cout << endl;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (j == 0)
				cout << "#";
			if (i == y && j == x)
				cout << "O";
			else if (i == fruitY && j == fruitX)
				cout << "F";
			else
				cout << " ";

			if (j == width - 1)
				cout << "#";
		}
		cout << endl;
	}
	for (int i = 0; i < width + 2; i++)
		cout << "#";
	cout << endl;
	cout << "score:" << score << endl;

}
void Input()
{
	if (_kbhit())
	{
		switch (_getch())
		{
			case 'a' :
				dir = LEFT;
				break;
			case 'd' :
				dir = RIGHT;
				break;
			case 'w' :
				dir = UP;
				break;
			case 's' :
				dir = DOWN;
				break;
			case 'x' :
				gameover = true;
				break;
		}
	}
}
void logic()
{
	switch (dir)
	{
	case LEFT:
		x--;
		break;
	case RIGHT:
		x++;
		break;
	case UP:
		y--;
		break;
	case DOWN:
		y++;
		break;
	default:
		break;
	}
}

int main()
{
	setup();
	while (!gameover)
	{
		Draw();
		Input();
		logic();
		_sleep(50);

	}
	return 0;
}
